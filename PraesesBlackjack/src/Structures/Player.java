package Structures;

import java.util.ArrayList;

/**
 * The basic player structure for the Blackjack game.
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 */
public class Player {

    private static final int STARTING_CHIPS = 2000;
    private final ArrayList<Card> hand = new ArrayList<>();
    private final ArrayList<Card> splithand = new ArrayList<>();

    private int chips = STARTING_CHIPS;
    private final String name;

    public Player() {
        this.name = "Player";
    }

    public Player(String name) {
        assert (name != null && !name.isEmpty());
        this.name = name;
    }

    /**
     * @return a string representation of all hands held by the player.
     */
    public String allHandsToString() {
        StringBuilder result = new StringBuilder(name + "'s hands:\n");
        if (!hand.isEmpty()) {
            if (!splithand.isEmpty()) {
                result.append("Main Hand: \n");
            }
            for (Card card : hand) {
                result.append(" ").append(card.toString()).append(",");
            }
            result.deleteCharAt(result.length() - 1); //Delete extra comma.
        }
        if (!splithand.isEmpty()) {
            assert (!hand.isEmpty());
            result.append("\nSplit Hand: \n ");
            for (Card card : splithand) {
                result.append(" ").append(card.toString()).append(",");
            }
            result.deleteCharAt(result.length() - 1);
        }
        return result.toString();
    }

    /**
     * @return the main hand
     */
    public ArrayList<Card> getMainHand() {
        assert (hand != null);
        return hand;
    }
    
    /**
     * Adds a card to the main hand.
     * @param c the card to add.
     */
    public void addCardMain(Card c) {
        hand.add(c);
    } 

    /**
     * @return the split hand
     */
    public ArrayList<Card> getSplithand() {
        assert (splithand != null);
        return splithand;
    }
    
    /**
     * Adds a card to the split hand.
     * @param c the card to add.
     */
    public void addCardSplit(Card c) {
        splithand.add(c);
    }
    
    /**
     * Splits the hand if both cards are of the same face value.  
     * Not yet implemented in game.
     */
    public void split() {
        assert(hand.size() == 2 && splithand.isEmpty());
        splithand.add(hand.remove(1));
    }
    
    /**
     * @return the chips
     */
    public int getChips() {
        return chips;
    }

    /**
     * @param chips the chips to add
     */
    public void addChips(int chips) {
        this.chips += chips;
    }

    /**
     * Clears all hands, returning the cards for use in a deck.
     * @return the cards in all hands of the player.
     */
    public ArrayList<Card> clearHands(){
        ArrayList<Card> temp = new ArrayList<>();
        for(int i = hand.size(); 0 < i; i--) {
            temp.add(hand.remove(0));
        }
        for(int i = splithand.size(); 0 < i; i--) {
            temp.add(splithand.remove(0));
        }
        return temp;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public String toString() {
        return String.format("Player Name: %s%nChip Count: %d", name, chips);
    }

}
