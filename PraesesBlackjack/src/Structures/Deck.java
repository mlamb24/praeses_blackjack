package Structures;

import java.security.SecureRandom;
import java.util.ArrayList;

/**
 * A deck composed of n standard decks of cards which requires no shuffling.
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 2014-4-20
 */
public class Deck {

    private ArrayList<Card> cards;
    public static SecureRandom randGen = new SecureRandom();

    /**
     * Creates a deck composed of n standard card decks.
     *
     * @param n The number of standard decks contained in the card pile.
     */
    public Deck(short n) {
        assert(n>0);
        if (n <= 0) {
            throw new DeckNumException("Card decks must be made up of a positive multiple of standard decks.");
        }
        this.cards = new ArrayList<>();
        for (; 0 < n; n--) {
            generateStdCards();
        }
    }

    /**
     * Generates a standard deck of cards, with one card of each suit and
     * denomination.
     */
    private void generateStdCards() {
        for (Card.Suit s : Card.Suit.values()) {
            for (Card.Name n : Card.Name.values()) {
                cards.add(new Card(n, s));
            }
        }
    }

    /**
     * Chooses a random integer and "draws" the Card in that position, removing
     * it from the deck.
     *
     * @return the Card removed from the deck.
     */
    public Card drawCard() {
        int i = randGen.nextInt(cards.size());
        return cards.remove(i);
    }

    /**
     * Adds a list of cards to the deck.
     * @param cards the list of cards to add.
     */
    public void addCards(ArrayList<Card> cards) {
        this.cards.addAll(cards);
    }
    
    /**
     * @return the number of cards left in the deck.
     */
    public int cardsLeft() {
        return cards.size();
    }
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Printing deck:");
        if (cards.isEmpty()) {
            return result.append("\nDeck empty.").toString();
        }
        for (Card c : cards) {
            result.append("\n").append(c);
        }
        return result.toString();
    }
}
