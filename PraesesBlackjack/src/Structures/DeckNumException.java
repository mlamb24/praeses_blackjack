
package Structures;

/**
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 */
public class DeckNumException extends RuntimeException {
    DeckNumException(String detailMessage) {
        super(detailMessage);
    }
}
