package Structures;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;

/**
 * Basic data structure for a card.
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 2014-4-20
 */
public final class Card {

    /**
     * @return the WIDTH
     */
    public static int getWIDTH() {
        return WIDTH;
    }

    /**
     * @return the HEIGHT
     */
    public static int getHEIGHT() {
        return HEIGHT;
    }

    /**
     * @return the name
     */
    public Name getName() {
        return name;
    }

    /**
     * The valid card names and their String representations.
     */
    public enum Name {

        ACE("Ace"), TWO("2"), THREE("3"), FOUR("4"),
        FIVE("5"), SIX("6"), SEVEN("7"),
        EIGHT("8"), NINE("9"), TEN("10"), JACK("Jack"),
        QUEEN("Queen"), KING("King");

        final private String name;

        private Name(String name) {
            this.name = name;
        }

        public String toString() {
            return name;
        }
    }

    /**
     * The valid card suits and their String representations.
     */
    public enum Suit {

        HEARTS("Hearts"), SPADES("Spades"), CLUBS("Clubs"), DIAMONDS("Diamonds");

        final private String suit;

        private Suit(String suit) {
            this.suit = suit;
        }

        public String toString() {
            return suit;
        }
    }

    //Allows us to only load each image once, even if there are multiple copies of a card.
    private static final HashMap<String, BufferedImage> IMAGE_LIBRARY = new HashMap<>();
    private static final String DEFAULT_IMAGE_PATH = "..\\Assets\\";
    private static final String DEFAULT_BACK_PATH = "..\\Assets\\Back.png";
    private static final int WIDTH = 72;
    private static final int HEIGHT = 100;

    private BufferedImage frontImage;
    private static BufferedImage backImage;

    private final Name name;
    private final Suit suit;
    private final String key;
    //private final byte deckID; Useful if we want multiple decks.

    /**
     * Creates a card of a given name and suit.
     *
     * @param name The card's name.
     * @param suit The card's suit.
     */
    public Card(Name name, Suit suit) {
        key = suit.toString() + name.toString();
        if (IMAGE_LIBRARY.containsKey(key)) {
            frontImage = IMAGE_LIBRARY.get(key);
        } else {
            try {
                IMAGE_LIBRARY.put(key, ImageIO.read(getClass().getResource(
                                DEFAULT_IMAGE_PATH + suit.toString() + "\\" + name.toString() + ".png")));
                frontImage = IMAGE_LIBRARY.get(key);
            } catch (IOException ex) {
                frontImage = null;
                ex.printStackTrace(System.err);
            }
        }


        this.name = name;
        this.suit = suit;
    }
    
    public BufferedImage getFrontImage() {
        return frontImage;
    }

    public static BufferedImage getBackImage() {
        if (backImage == null) {
            try {
                backImage = ImageIO.read(Card.class.getResource(DEFAULT_BACK_PATH));
            } catch (IOException ex) {
                backImage = null;
                ex.printStackTrace(System.err);
            }
        }
        return backImage;
    }
    /**
     * Compares a card's name for equality.
     *
     * @param c the card to compare to
     * @return true if the names are the same, false otherwise.
     */
    public boolean compareName(Card c) {
        return c.getName() == this.getName();
    }

    /**
     * Compares a card's suit for equality.
     *
     * @param c the card to compare to
     * @return true if the suits are the same, false otherwise.
     */
    public boolean compareSuit(Card c) {
        return c.suit == this.suit;
    }

    /**
     * Compares a card's name and suit for equality. To test for suit or name
     * matches use compareSuit() and compareName(), respectively.
     *
     * @param c the card to compare to.
     * @return true if the two cards are identical, false otherwise.
     */
    public boolean equals(Card c) {
        return compareName(c) && compareSuit(c);
    }

    /**
     * @return a String representation in the form "name of suit".
     */
    public String toString() {
        return getName() + " of " + suit;
    }

}
