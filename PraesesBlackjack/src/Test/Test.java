package Test;

import Structures.Card;
import Structures.Card.Name;
import Structures.Card.Suit;
import Structures.Deck;
import Structures.Player;
import java.util.ArrayList;

/**
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 */
public class Test {

    public static void main(String[] args) {
        deckTest();
        scoreTest();
    }

    private static void deckTest() {
        Deck deck = new Deck((short)1);
        System.out.println(deck);
        ArrayList<Card> draw = new ArrayList<>();
        for (int i = 0; 52 > i; i++) {
            try {
                draw.add(deck.drawCard());
            } catch (IndexOutOfBoundsException e) {
                System.err.println("deckTest failed 23: Ran out of cards.");
                return;
            }
        }
        System.out.println(deck.toString());
        if (!deck.toString().equals("Printing deck:\nDeck empty.")) {
            System.err.println("deckTest failed 29: Unexpected toString() from empty deck.");
            return;
        }
        if (draw.size() != 52) {
            System.err.println("Unexpected amount of cards after drawing full deck. " + draw.size() );
            return;
        }
        System.out.println(draw.toString());
        System.out.println("deckTest passed.");
    }
    
    private static void scoreTest() {
        Player testPlayer = new Player("Test");
        Player dealer = new Player("Dealer");
        
        testPlayer.addCardMain(new Card(Name.KING,Suit.DIAMONDS));
        testPlayer.addCardMain(new Card(Name.ACE,Suit.DIAMONDS));
        dealer.addCardMain(new Card(Name.TWO,Suit.DIAMONDS));
        dealer.addCardMain(new Card(Name.SIX,Suit.DIAMONDS));
        
        if (calculateVictor(testPlayer, dealer) != true) {
            System.err.println("Unexpected victor. ");
            return;
        }
        
        System.out.println("ScoreTest passed");
    }
    
    public static boolean calculateVictor(Player one, Player dealer) {
        int playerScore = scoreHand(one.getMainHand());
        int dealerScore = scoreHand(dealer.getMainHand());
        return !checkBust(one.getMainHand()) 
                && (playerScore >= dealerScore || checkBust(dealer.getMainHand()));
    }
    
    private static int scoreHand(ArrayList<Card> hand) {
        ArrayList<Integer> scores = new ArrayList<>();
        scores.add(0);
        for (Card c : hand) {
            int numScores = scores.size();
            if (c.getName() == Card.Name.ACE) {
                for (int i = 0; i < numScores; i++) {
                    scores.set(i, scores.get(i) + 1);
                }
                for (int i = 0; i < numScores; i++) {
                    scores.add(scores.get(i) + 10);
                }
            } else if (c.getName() == Card.Name.KING || c.getName() == Card.Name.QUEEN || c.getName() == Card.Name.JACK) {
                for (int i = 0; i < numScores; i++) {
                    scores.set(i, scores.get(i) + 10);
                }
            } else {
                int n = Integer.parseInt(c.getName().toString());
                for (int i = 0; i < numScores; i++) {
                    scores.set(i, scores.get(i) + n);
                }
            }
        }
        int score = 0;
        for (int i = 0; i < scores.size(); i++) {
            if (i == 0) {
                score = scores.get(i);
            } else {
                int n = scores.get(i);
                if (n > score && n <= 21) {
                    score = n;
                }
            }
        }
        return score;
    }
    
    public static boolean checkBust(ArrayList<Card> hand) {
        return (scoreHand(hand) > 21);
    }
}
