package Graphics;

import Graphics.BlackJackViewer.Buttons;
import Structures.Card;
import Structures.Card.Name;
import Structures.Card.Suit;
import Structures.Deck;
import Structures.Player;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 * The panel in which the blackjack game is played.  Contains a mix of game code
 * and graphics code to avoid obfuscation by separating the two.
 * 
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 */
public class Game extends JPanel {

    private static final int WIDTH = 643;
    private static final int HEIGHT = 344;
    
    private static final short NUM_DECKS = 1;
    private static final int BET_AMT = 100;
    
    private static final int CARD_COUNT = Suit.values().length * Name.values().length * NUM_DECKS;
    private Deck deck = new Deck(NUM_DECKS);
    
    private final Player dealer = new Player("Dealer");
    private final Player player1;

    //State Variables
    private boolean playing = false;
    private boolean gameOver = false;
    private boolean broke = false;
    private boolean dubDown = false;
    //private boolean firstTurn = false;
    private boolean dealerDone = false;
    //private boolean split = false;

    private final BlackJackViewer controller;

    public Game(BlackJackViewer controller) {
        setBackground(new Color(0, 66, 33));
        player1 = new Player(BlackJackMenu.getPlayerName());
        this.controller = controller;
        controller.disableButton(Buttons.SPLIT); //Split not yet implemented.
        updateButtons();
        repaint();
    }

    /**
     * Deals cards and resets state values.
     */
    public void deal() {
        //Refills deck
        if (deck.cardsLeft() != CARD_COUNT) {
            deck.addCards(dealer.clearHands());
            deck.addCards(player1.clearHands());
        }

        if (player1.getChips() > BET_AMT) {
            broke = false;
        } else {
            broke = true;
        }
        
        if (broke) {
            playing = false;
            gameOver = true;
            repaint();
            return;
        }
        
        player1.addChips(-BET_AMT);
        controller.updateChips();
        
        dealer.addCardMain(deck.drawCard());
        player1.addCardMain(deck.drawCard());
        dealer.addCardMain(deck.drawCard());
        player1.addCardMain(deck.drawCard());

        //State variables
        playing = true;
        //firstTurn = true;
        gameOver = false;
        dealerDone = false;
        dubDown = false;
        
        if (scoreHand(player1.getMainHand()) == 21) {
            gameEnd(true);
        }
        if (scoreHand(dealer.getMainHand()) == 21) {
            gameEnd(false);
        }
        
        repaint();
        updateButtons();

    }

    /**
     * Take another card.
     */
    public void hit() {
        //if (firstTurn) {
        //    firstTurn = false;
        //}
        player1.addCardMain(deck.drawCard());
        repaint();
        if (checkBust(player1.getMainHand())) {
            gameEnd(false);
            return;
        }
        if (!dealerDone) {
            dealerAI();
        }
        updateButtons();
    }

    /**
     * Finishes your hand, allowing the dealer to do as they please before the
     * game is decided.
     */
    public void stand() {
        //if (firstTurn) {
        //    firstTurn = false;
        //}
        repaint();
        while (!dealerDone) {
            dealerAI();
        }
        gameEnd(calculateVictor());
    }

    /**
     * SPLIT NOT YET IMPLEMENTED
     */
    public void split() {
        /*    if (canSplit()) {
         player1.split();
         firstTurn = false;
         }*/
    }

    /*private boolean canSplit() {
     ArrayList<Card> hand = getPlayer().getMainHand();
     return firstTurn && hand.size() == 2 && hand.get(0).compareName(hand.get(1));
     }*/
    
    /**
     * Double your bet and stand after one more hit.
     */
    public void doubleDown() {
        /*if (firstTurn) {
         firstTurn = false;
         }*/
        dubDown = true;
        player1.addChips(-BET_AMT);
        player1.addCardMain(deck.drawCard());
        repaint();
        if (checkBust(player1.getMainHand())) {
            gameEnd(false);
            return;
        }
        stand();
    }

    public boolean checkBust(ArrayList<Card> hand) {
        return (scoreHand(hand) > 21);
    }

    public void gameEnd(boolean won) {
        playing = false;
        gameOver = true;
        if (won) {
            if (dubDown) {
                player1.addChips(3 * BET_AMT);

            } else {
                player1.addChips(2 * BET_AMT);
            }
        }
        controller.updateChips();
        updateButtons();
    }

    /**
     * Returns whether the player or the dealer have a higher scoring hand.
     * @return true if the dealer busts or the player has a higher scoring hand
     * under 21.
     */
    public boolean calculateVictor() {
        int playerScore = scoreHand(player1.getMainHand());
        int dealerScore = scoreHand(dealer.getMainHand());
        return !checkBust(player1.getMainHand()) 
                && (playerScore >= dealerScore || checkBust(dealer.getMainHand()));
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D graphics = (Graphics2D) g;
        
        //Game Over messages
        if (gameOver) {
            String finalMsg;
            if (broke) {
                finalMsg = "You're out of money!";
            } else if (calculateVictor()) {
                finalMsg = "You Win!";
            } else {
                finalMsg = "You Lost!";
            }
            Font small = new Font("Helvetica", Font.BOLD, 14);
            FontMetrics metr = this.getFontMetrics(small);

            g.setColor(Color.white);
            g.setFont(small);
            g.drawString(finalMsg, (WIDTH - metr.stringWidth(finalMsg)) / 2,
                    HEIGHT / 2);
        }

        //Lets us overlap the cards.
        int drawX = WIDTH / 2 - dealer.getMainHand().size() * (Card.getWIDTH() / 3) / 2;
        if (!dealer.getMainHand().isEmpty()) {
            if (!gameOver) {
                graphics.drawImage(Card.getBackImage(), drawX, 12, this);
            } else {
                graphics.drawImage(dealer.getMainHand().get(0).getFrontImage(), drawX, 12, this);
            }
            for (int i = 1; dealer.getMainHand().size() > i; i++) {
                graphics.drawImage(dealer.getMainHand().get(i).getFrontImage(), drawX + i * (Card.getWIDTH() / 3), 12, this);
            }
        }
        
        drawX = WIDTH / 2 - player1.getMainHand().size() * (Card.getWIDTH() / 3) / 2;
        if (!player1.getMainHand().isEmpty()) {
            for (int i = 0; player1.getMainHand().size() > i; i++) {
                graphics.drawImage(player1.getMainHand().get(i).getFrontImage(), drawX + i * (Card.getWIDTH() / 3), HEIGHT - (12 + Card.getHEIGHT()), this);
            }
        }
    }

    /**
     * Enables or disables buttons based on current state.
     */
    public void updateButtons() {
        if (playing) {
            controller.enableButton(Buttons.HIT);
            controller.enableButton(Buttons.STAND);
            if (player1.getChips() > BET_AMT) {
                controller.enableButton(Buttons.DOUBLE);
            }
            controller.disableButton(Buttons.STARTHAND);
        } else {
            controller.disableButton(Buttons.HIT);
            controller.disableButton(Buttons.STAND);
            controller.disableButton(Buttons.DOUBLE);
            controller.enableButton(Buttons.STARTHAND);
        }
    }

    /**
     * Calculates the best possible score for a hand.  Decides on ace values
     * automatically.
     * 
     * @param hand the hand to be scored.
     * @return the best possible score of hand.
     */
    private int scoreHand(ArrayList<Card> hand) {
        ArrayList<Integer> scores = new ArrayList<>();
        scores.add(0);
        for (Card c : hand) {
            int numScores = scores.size();
            if (c.getName() == Card.Name.ACE) {
                for (int i = 0; i < numScores; i++) {
                    scores.set(i, scores.get(i) + 1);
                }
                for (int i = 0; i < numScores; i++) {
                    scores.add(scores.get(i) + 10);
                }
            } else if (c.getName() == Card.Name.KING || c.getName() == Card.Name.QUEEN || c.getName() == Card.Name.JACK) {
                for (int i = 0; i < numScores; i++) {
                    scores.set(i, scores.get(i) + 10);
                }
            } else {
                int n = Integer.parseInt(c.getName().toString());
                for (int i = 0; i < numScores; i++) {
                    scores.set(i, scores.get(i) + n);
                }
            }
        }
        int score = 0;
        for (int i = 0; i < scores.size(); i++) {
            if (i == 0) {
                score = scores.get(i);
            } else {
                int n = scores.get(i);
                if (n > score && n <= 21) {
                    score = n;
                }
            }
        }
        return score;
    }

    /**
     * Current dealer AI always hits if hand is worth less than 18.
     */
    private void dealerAI() {
        if (scoreHand(dealer.getMainHand()) > 17) {
            dealerDone = true;
        } else {
            dealer.addCardMain(deck.drawCard());
            if (checkBust(dealer.getMainHand())) {
                dealerDone = true;
                gameEnd(true);
            }
            repaint();


        }
    }

    /**
     * @return the player
     */
    public Player getPlayer() {
        return player1;
    }
}
