package Graphics;

/**
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 */
public class BlackJackViewer extends javax.swing.JFrame {

    /**
     * Creates new form BlackJackViewer
     */
    public BlackJackViewer() {
        initComponents();
        setResizable(false);
        java.awt.Dimension dimension
                = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((int) (dimension.getWidth() - this.getWidth()) / 2,
                (int) (dimension.getHeight() - this.getHeight()) / 2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {

        HitButton = new javax.swing.JButton();
        SplitButton = new javax.swing.JButton();
        QuitButton = new javax.swing.JButton();
        DoubleButton = new javax.swing.JButton();
        StandButton = new javax.swing.JButton();
        NameLabel = new javax.swing.JLabel();
        ChipsLabel = new javax.swing.JLabel();
        startHandButton = new javax.swing.JButton();
        game = new Game(this);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        HitButton.setText("Hit");
        HitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game.hit();
            }
        });
        
        SplitButton.setText("Split");
        SplitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game.split();
            }
        });

        QuitButton.setText("Quit");
        QuitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exit();
            }
        });

        DoubleButton.setText("Double");
        DoubleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game.doubleDown();
            }
        });

        StandButton.setText("Stand");
        StandButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game.stand();
            }
        });

        NameLabel.setText("Name: " + game.getPlayer().getName());

        ChipsLabel.setText("Chips Left: " + game.getPlayer().getChips());

        startHandButton.setText("Start Hand");
        startHandButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                game.deal();
            }
        });

        javax.swing.GroupLayout TableLayout = new javax.swing.GroupLayout(game);
        game.setLayout(TableLayout);
        TableLayout.setHorizontalGroup(
                TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 0, Short.MAX_VALUE)
        );
        TableLayout.setVerticalGroup(
                TableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 344, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(game, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(NameLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                        .addComponent(HitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(StandButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(ChipsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(SplitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addComponent(DoubleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(QuitButton, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                                                .addComponent(startHandButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(game, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(HitButton, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                                .addComponent(SplitButton, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                                .addComponent(QuitButton, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                                .addComponent(DoubleButton, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                                .addComponent(StandButton, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(NameLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ChipsLabel))
                                .addComponent(startHandButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
        );

        pack();
    }

    private void exit() {
        this.dispose();
        BlackJackMenu.run();
    }

    public void updateChips() {
        ChipsLabel.setText("Chips Left: " + game.getPlayer().getChips());
    } 
    /**
     *
     * @param b The button to disable
     */
    public void disableButton(Buttons b) {
        switch (b) {
            case STARTHAND:
                startHandButton.setEnabled(false);
                break;
            case HIT:
                HitButton.setEnabled(false);
                break;
            case STAND:
                StandButton.setEnabled(false);
                break;
            case SPLIT:
                SplitButton.setEnabled(false);
                break;
            case DOUBLE:
                DoubleButton.setEnabled(false);
                break;
            case QUIT:
                DoubleButton.setEnabled(false);
                break;
            default:
                throw new Error("Invalid Button");
        }
    }

    /**
     *
     * @param b The button to disable
     */
    public void enableButton(Buttons b) {
        switch (b) {
            case STARTHAND:
                startHandButton.setEnabled(true);
                break;
            case HIT:
                HitButton.setEnabled(true);
                break;
            case STAND:
                StandButton.setEnabled(true);
                break;
            case SPLIT:
                SplitButton.setEnabled(true);
                break;
            case DOUBLE:
                DoubleButton.setEnabled(true);
                break;
            case QUIT:
                DoubleButton.setEnabled(true);
                break;
            default:
                throw new Error("Invalid Button");
        }
    }
    private Game game;
    private javax.swing.JLabel ChipsLabel;
    private javax.swing.JButton DoubleButton;
    private javax.swing.JButton HitButton;
    private javax.swing.JLabel NameLabel;
    private javax.swing.JButton QuitButton;
    private javax.swing.JButton SplitButton;
    private javax.swing.JButton StandButton;
    private javax.swing.JButton startHandButton;

    public enum Buttons {

        STARTHAND, HIT, STAND, SPLIT, DOUBLE, QUIT;
    }
}
